$(function(){
  
  //ハンバーガーメニュー
  var state = false;
  var scrollpos;
  $('.hamburger_icon').on('click', function() {
    $(this).toggleClass('open');
    $(".l-header .navi").fadeToggle();
    $(".sp_menu_bg").fadeToggle();
    if(state == false) {
      scrollpos = $(window).scrollTop();
      $('body').addClass('fixed').css({'top': -scrollpos});
      state = true;
    } else {
      $('body').removeClass('fixed').css({'top': 0});
      window.scrollTo( 0 , scrollpos );
      state = false;
    }
    return false;
  });
  var w = $(window).width();
	var x = 799;
  //スマホ時のみ動作
	if (w <= x) {
    $('.menu li a').on('click', function() {
      $(".navi").fadeOut();
      $(".hamburger_icon").removeClass("open");
    });
  } else {  //PC時のみ動作
    //タブ切り替え
    $('.ChangeElem_Btn').each(function () {
      $(this).on('click', function () {
        var index = $('.ChangeElem_Btn').index(this);
        $('.ChangeElem_Btn').removeClass('active');
        $(this).addClass('active');
        $('.ChangeElem_Panel').removeClass('active');
        $('.ChangeElem_Panel').eq(index).addClass('active');
      });
    });
  }

  //別ページのアンカーリンク ?id=○○ で遷移
  $(window).on('load', function() {
    var url = $(location).attr('href');
    if(url.indexOf("?id=") != -1){
      var id = url.split("?id=");
      var $target = $('#' + id[id.length - 1]);
      if($target.length){
        var pos = $target.offset().top;
        $("html, body").animate({scrollTop:pos}, 1000);
      }
    }
  });

  //タブ切り替え②
  /*初期表示*/
  $('.ChangeElem_Panel2').hide();
  $('.ChangeElem_Panel2').eq(0).show();
  $('.ChangeElem_Btn2').eq(0).addClass('is-active');
  /*クリックイベント*/
  $('.ChangeElem_Btn2').each(function () {
    $(this).on('click', function () {
      var index = $('.ChangeElem_Btn2').index(this);
      $('.ChangeElem_Btn2').removeClass('active');
      $(this).addClass('active');
      $('.ChangeElem_Panel2').hide();
      $('.ChangeElem_Panel2').eq(index).fadeIn("slow");
    });
  });

  //カルーセル
  $('.main_slider').slick({
    infinite: true,
    dots: true,
    slidesToShow: 1,
    centerMode: true, //要素を中央寄せ
    centerPadding:'0', //両サイドの見えている部分のサイズ
    autoplay:true, //自動再生
    autoplaySpeed: 5000,
    fade: true,
    arrows: false,
  });

});

$(document).ready(function(){
  //URLのハッシュ値を取得
  var urlHash = location.hash;
  //ハッシュ値があればページ内スクロール
  if(urlHash) {
    //スクロールを0に戻す
    $('body,html').stop().scrollTop(0);
    setTimeout(function () {
      //ロード時の処理を待ち、時間差でスクロール実行
      scrollToAnker(urlHash) ;
    }, 100);
  }

  //通常のクリック時
  $('a[href^="#"]').click(function() {
    //ページ内リンク先を取得
    var href= $(this).attr("href");
    //リンク先が#か空だったらhtmlに
    var hash = href == "#" || href == "" ? 'html' : href;
    //スクロール実行
    scrollToAnker(hash);
    //リンク無効化
    return false;
  });

  // 関数：スムーススクロール
  // 指定したアンカー(#ID)へアニメーションでスクロール
  function scrollToAnker(hash) {
    var target = $(hash);
    var position = target.offset().top;
    $('body,html').stop().animate({scrollTop:position}, 500);
  }

})

var $win = $(window);
$win.on('load resize', function() {

  //画面の高さを取得して、変数wHに代入
  var wH = $(window).height(); 
  //要素の高さを取得を取得して、変数divHに代入
  var divH = $('#main_visual .mv_inner').innerHeight();

  if (window.matchMedia('(max-width:480px)').matches) {
    //SPの場合
    
  } else if (window.matchMedia('(max-width:834px)').matches) {
    //タブレットの場合
    
  } else {
    //PCの場合
    if(wH > divH){
      //$('#main_visual .mv_inner').css('height',wH -80 + 'px'); 
    }
  };

});